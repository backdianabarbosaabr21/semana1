package net.techu;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductosController {
    private ArrayList<String> listaProductos = null;

    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }

    /* Get lista de productos */
    @RequestMapping(value = "/productos", method = RequestMethod.GET, produces = "application/json")
    public ArrayList<String> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return listaProductos;
    }
    @RequestMapping(value="/productos/{id}", method = RequestMethod.GET)
    public String obtenerProductoPorId(@PathVariable int id)
    {
        String resultado = null;
        try {
            resultado = listaProductos.get(id);
        }
        catch (Exception ex) {
            resultado = "No se ha encontrado el producto";
        }
        return resultado;
    }

    /* Add nuevo producto */
    @RequestMapping(value = "/productos", method = RequestMethod.POST, produces="application/json")
    public void AddProducto() {
        System.out.println("Estoy en añadir");
        listaProductos.add("NUEVO");
    }

    /* Add nuevo producto con nombre */
    @RequestMapping(value = "/productos/{nom}/{cat}", method = RequestMethod.POST, produces="application/json")
    public void AddProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(nombre);
    }
}


